package com.adaptivewrapper.service;

import com.adaptivewrapper.dto.SpecificationDTO;
import com.adaptivewrapper.entity.Specification;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SpecificationService {

    Flux<Specification> getAllSpecifications();
    Mono<Specification> getSpecificationById(String id);
    Mono<Specification> addSpecification(SpecificationDTO specificationDTO);
    Mono<Specification> updateSpecificationById(String id, SpecificationDTO specificationDTO);
    Mono<Void> deleteSpecificationById(String id);

}
