package com.adaptivewrapper.dto;

import com.mongodb.lang.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class SpecificationDTO {

    private String assessmentId;

    private String title;

    private Integer questionLimit;

    @Nullable
    private Float precision;

    private String model;

    @Nullable
    private Float initialAbility;

    @Nullable
    private String itemSelector;

    @Nullable
    private String abilityEstimator;
}
