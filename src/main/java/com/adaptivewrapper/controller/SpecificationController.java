package com.adaptivewrapper.controller;

import com.adaptivewrapper.dto.SpecificationDTO;
import com.adaptivewrapper.entity.Specification;
import com.adaptivewrapper.service.SpecificationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("v1/spec")
public class SpecificationController {

    @Autowired
    private SpecificationServiceImpl service;

    @PostMapping
    public Mono<Specification> addSpecification(@RequestBody SpecificationDTO dto) {
        return service.addSpecification(dto);
    }

    @GetMapping("/{id}")
    public Mono<Specification> getSpecificationById(@PathVariable String id) {
        return service.getSpecificationById(id);
    }

    @GetMapping
    public Flux<Specification> getAllSpecifications() {
        return service.getAllSpecifications();
    }

    @PutMapping("/{id}")
    public Mono<Specification> update(@PathVariable String id, @RequestBody SpecificationDTO dto) {
        return service.updateSpecificationById(id, dto);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable String id) {
        return service.deleteSpecificationById(id);
    }

}
