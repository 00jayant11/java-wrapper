package com.adaptivewrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mohammad.matin
 *
 */

@SpringBootApplication
public class AdaptiveWrapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdaptiveWrapperApplication.class, args);
	}

}
