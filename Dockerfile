stages:
  - build
  - deploy
  - service

variables:
  BUCKET_NAME: "cat-demo-artifacts-new"
  REPO_NAME: "cat-JAVA-WRAPPER"
  KEY_NAME: "$CI_PIPELINE_ID.zip"

maven-build-job:
  stage: build
  before_script:
    - java -version
    - mvn -version
    
  script:
    - JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/  mvn clean install -DskipTests 
    - zip $KEY_NAME target/*.jar -j
    
  artifacts:
    name: "java-build"
    paths: 
      - $KEY_NAME
  only:
    - develop
    - main
  tags:
    - java
    
s3-deploy:
  stage: deploy
  script: 
    - aws s3 cp $KEY_NAME s3://$BUCKET_NAME/$REPO_NAME/
    - echo $(aws s3api put-object-tagging --bucket $BUCKET_NAME --key $REPO_NAME/$KEY_NAME --tagging 'TagSet=[{Key=Organization,Value=Tech},{Key=Project,Value=JAVA}]')
  dependencies:
  - maven-build-job
  only:
    - develop
    - main
  tags:
    - java

start-java-service:
  stage: service
  before_script:
    - sudo mkdir /home/ubuntu/adaptive-assessment/cat-java-wrapper/new
    - sudo aws s3 cp s3://$BUCKET_NAME/$REPO_NAME/$KEY_NAME /home/ubuntu/adaptive-assessment/cat-java-wrapper/new/
    - sudo unzip /home/ubuntu/adaptive-assessment/cat-java-wrapper/new/$KEY_NAME -d /home/ubuntu/adaptive-assessment/cat-java-wrapper/new
  
  script:
    - sh -c '/home/ubuntu/adaptive-assessment/start_java_wrapper.sh'
    # - "for pid in $(sudo lsof -t -i:8081); do sudo kill -9 $pid; done"

  # script:
  #   - nohup java -jar /home/ubuntu/adaptive-assessment/cat-services-api/*.jar > /dev/null 2>&1 &
  dependencies:
   - maven-build-job
  only:
    - develop
    - main
  tags:
    - java

